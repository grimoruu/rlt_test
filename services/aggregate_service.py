from datetime import datetime

from dateutil.relativedelta import relativedelta

from config.mongo_settings import collection


def aggregate_salaries(dt_from: str, dt_upto: str, group_type: str) -> dict:
    dataset = []
    labels = []
    dtt_from = datetime.fromisoformat(dt_from)
    dtt_upto = datetime.fromisoformat(dt_upto)
    current_date: datetime = dtt_from

    if group_type == "hour":
        delta = relativedelta(hours=1)
    elif group_type == "day":
        delta = relativedelta(days=1)
    elif group_type == "month":
        delta = relativedelta(months=1)
    else:
        raise ValueError("Неверный тип агрегации")

    while current_date <= dtt_upto:
        next_date = current_date + delta
        total_salary = sum(aggregate_data(current_date, next_date))
        dataset.append(total_salary)
        labels.append(current_date.isoformat())
        if group_type == "hour" and current_date == dtt_upto:
            dataset[-1] = 0
        if group_type == "day" and current_date == dtt_upto:
            dataset[-1] = 0
        current_date = next_date
    return {"dataset": dataset, "labels": labels}


def aggregate_data(current_date: datetime, next_date: datetime) -> list:
    items = list(
        collection.aggregate(
            [
                {"$match": {"dt": {"$gte": current_date, "$lt": next_date}}},
                {
                    "$group": {
                        "_id": {
                            "year": {"$year": "$date"},
                            "month": {"$month": "$date"},
                            "day": {"$dayOfMonth": "$date"},
                            "hour": {"$hour": "$date"},
                        },
                        "total_salary": {"$sum": "$value"},
                    }
                },
            ]
        )
    )
    return [item["total_salary"] for item in items]
