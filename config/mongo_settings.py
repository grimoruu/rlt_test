import pymongo
from pymongo import MongoClient

client: MongoClient = pymongo.MongoClient("mongodb://localhost:27017")
db = client["sampleDB"]
collection = db["sample_collection"]
