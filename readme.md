# ТГ-бот для агрегации статистических данных

### Установка и использование

1. Клонирование репозитория:

```bash
git clone https://gitlab.com/grimoruu/rlt_test.git
```

2. Директория проекта:

```bash
cd rlt_test
```

3. Установите зависимости poetry:

```bash
poetry install
```

4. Docker compose для MongoDB:

```bash
docker compose up -d
```

5. Заполните бд тестовыми данными, для этого перекиньте папку sampleDB 
с дампом в корневой каталог проекта и запустите следующую команду
```bash
mongorestore -d sampleDB sampleDB

```

6. Запуск приложения:

```bash
python3 main.py
```

7. Бот будет доступен по адресу tg @rlt_test_v2_bot
8. Примеры входных данных:
```bash
{"dt_from": "2022-10-01T00:00:00", "dt_upto": "2022-11-30T23:59:00", "group_type": "day"}

```
```bash
{"dt_from": "2022-09-01T00:00:00", "dt_upto": "2022-12-31T23:59:00", "group_type": "month"}

```