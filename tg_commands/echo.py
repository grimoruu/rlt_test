import json

from pymongo import MongoClient
from telegram import Update
from telegram.ext import ContextTypes

from services.aggregate_service import aggregate_salaries


async def echo_command(update: Update, context: ContextTypes.DEFAULT_TYPE, client: MongoClient) -> None:
    try:
        json_data = update.message.text.replace("\xa0", " ")
        data = json.loads(json_data)
        dt_from = data["dt_from"]
        dt_upto = data["dt_upto"]
        group_type = data["group_type"]
        answer = aggregate_salaries(dt_from, dt_upto, group_type)
        await update.message.reply_text(json.dumps(answer))
    except json.JSONDecodeError:
        await update.message.reply_text("Ошибка: Неверный формат JSON.")
    except KeyError as e:
        await update.message.reply_text(f"Ошибка: Отсутствует ключ {e}.")
    except Exception as e:
        await update.message.reply_text(f"Произошла ошибка: {e}")
