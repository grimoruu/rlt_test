from telegram import Update
from telegram.ext import ApplicationBuilder, MessageHandler, filters

from config.mongo_settings import client
from config.settings import TOKEN
from tg_commands.echo import echo_command

if __name__ == "__main__":
    try:
        application = ApplicationBuilder().token(TOKEN).build()
        application.add_handler(
            MessageHandler(
                filters.TEXT & ~filters.COMMAND, lambda update, context: echo_command(update, context, client)
            )
        )
        application.run_polling(allowed_updates=Update.ALL_TYPES)
    finally:
        client.close()
